<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cursos".
 *
 * @property int $id
 * @property string $nombre
 * @property string $especialidad
 * @property string $nivel
 * @property string $dirigido
 * @property string $modalidad
 * @property string $turno
 * @property string $cartel
 * @property string $informacion
 *
 * @property Matriculas[] $matriculas
 * @property Alumnos[] $alumnos
 */
class Cursos extends \yii\db\ActiveRecord {

    public $actualizarCartel;
    public $actualizarInformacion;
    
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'cursos';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['nombre', 'especialidad', 'nivel', 'dirigido', 'modalidad', 'turno'], 'string', 'max' => 255],
            [['cartel'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            [['informacion'], 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'especialidad' => 'Especialidad',
            'nivel' => 'Nivel',
            'dirigido' => 'Dirigido',
            'modalidad' => 'Modalidad',
            'turno' => 'Turno',
            'cartel' => 'Cartel',
            'informacion' => 'Informacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMatriculas() {
        return $this->hasMany(Matriculas::className(), ['curso' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlumnos() {
        return $this->hasMany(Alumnos::className(), ['id' => 'alumno'])->viaTable('matriculas', ['curso' => 'id']);
    }

    public function beforeSave($insert) {
        /* voy a utilizar estas dos propiedades para saber si el usuario a actualizado los ficheros subidos*/
        $this->actualizarInformacion=TRUE;
        $this->actualizarCartel=TRUE;
        /* si no ha subido ningun fichero recupero el valor anterior de los ficheros */
        if(!isset($this->informacion)){
            $this->informacion=$this->getOldAttribute("informacion");
            $this->actualizarInformacion=FALSE;
        }
        if(!isset($this->cartel)){
            $this->cartel=$this->getOldAttribute("cartel");
            $this->actualizarCartel=FALSE;
        }
        return true;
    }
    
    

    public function afterSave($insert, $changeAttributes) {

        /* comprobar si ha seleccionado algun archivo */
        /* ademas comprobamos que sea un fichero lo que me llegue*/
        /* esto es para que si estoy borrando no me de problemas */
        /* llegaria un string en ese caso */
        
        if($this->actualizarInformacion && is_object($this->informacion)){
            //crear directorio
            \yii\helpers\FileHelper::createDirectory('imgs/informacion/' . $this->id , 0775, false);
            $this->informacion->saveAs('imgs/informacion/' . $this->id . "/" . iconv('UTF-8','ISO-8859-1',$this->informacion->name), false);
            $this->informacion = $this->informacion->name;
        }
        
        /* comprobar si ha seleccionado algun archivo */
        /* ademas comprobamos que sea un fichero lo que me llegue*/
        /* esto es para que si estoy borrando no me de problemas */
        /* llegaria un string en ese caso */
        if($this->actualizarCartel && is_object($this->cartel)){
            //crear directorio
            \yii\helpers\FileHelper::createDirectory('imgs/cartel/' . $this->id , 0775, false);
            // la funcion iconv la utilizo para que saveAs no me de problemas con las tildes y ñ
            $this->cartel->saveAs('imgs/cartel/' . $this->id . "/" . iconv('UTF-8','ISO-8859-1',$this->cartel->name), false);
            $this->cartel = $this->cartel->name;
        }
        //almacenar los cambios
        $this->updateAttributes(["informacion","cartel"]);
    }

    public function getCartel() {
        return Yii::$app->request->getBaseUrl() . '/imgs/cartel/' . $this->id . "/" . $this->cartel;
    }

    public function getInformacion() {
        return Yii::$app->request->getBaseUrl() . '/imgs/informacion/' . $this->id . "/" . $this->informacion;
    }

}
