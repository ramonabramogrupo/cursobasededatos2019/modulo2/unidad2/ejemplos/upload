﻿DROP DATABASE IF EXISTS upload;
CREATE DATABASE IF NOT EXISTS upload
	CHARACTER SET utf8
	COLLATE utf8_spanish_ci;


SET NAMES 'utf8';

-- 
-- Set default database
--
USE upload;

--
-- Definition for table alumnos
--
CREATE TABLE IF NOT EXISTS alumnos (
  id INT(11) NOT NULL AUTO_INCREMENT,
  nombre VARCHAR(255) DEFAULT NULL,
  apellidos VARCHAR(255) DEFAULT NULL,
  telefono VARCHAR(255) NOT NULL,
  email VARCHAR(50) NOT NULL,
  titulacion VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE INDEX UK_alumnos_email (email)
)
ENGINE = INNODB
AUTO_INCREMENT = 3
AVG_ROW_LENGTH = 8192
CHARACTER SET utf8
COLLATE utf8_spanish_ci;

--
-- Definition for table cursos
--
CREATE TABLE IF NOT EXISTS cursos (
  id INT(11) NOT NULL AUTO_INCREMENT,
  nombre VARCHAR(255) DEFAULT NULL,
  especialidad VARCHAR(255) DEFAULT NULL,
  nivel VARCHAR(255) DEFAULT NULL,
  dirigido VARCHAR(255) DEFAULT NULL,
  modalidad VARCHAR(255) DEFAULT NULL,
  turno VARCHAR(255) DEFAULT NULL,
  cartel VARCHAR(255) DEFAULT NULL,
  informacion VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 4
AVG_ROW_LENGTH = 5461
CHARACTER SET utf8
COLLATE utf8_spanish_ci;

--
-- Definition for table matriculas
--
CREATE TABLE IF NOT EXISTS matriculas (
  id INT(11) NOT NULL AUTO_INCREMENT,
  alumno INT(11) DEFAULT NULL,
  curso INT(11) DEFAULT NULL,
  fecha DATE DEFAULT NULL,
  fecha_fin DATE DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE INDEX UK_matriculas (alumno, curso),
  CONSTRAINT FK_matriculas_alumno FOREIGN KEY (alumno)
    REFERENCES alumnos(id) ON DELETE NO ACTION ON UPDATE RESTRICT,
  CONSTRAINT FK_matriculas_curso FOREIGN KEY (curso)
    REFERENCES cursos(id) ON DELETE NO ACTION ON UPDATE RESTRICT
)
ENGINE = INNODB
AUTO_INCREMENT = 5
AVG_ROW_LENGTH = 4096
CHARACTER SET utf8
COLLATE utf8_spanish_ci;

-- 
-- Dumping data for table alumnos
--
INSERT INTO alumnos VALUES
(1, 'ramon', 'abramo', '629629629', 'ramon@ramon.es', 'ingeniero'),
(2, 'jose', 'lopez lopez', '600600600', 'jose@jose.es', 'profesor');

-- 
-- Dumping data for table cursos
--
INSERT INTO cursos VALUES
(1, 'Desarrollo aplicaciones con tecnologias web', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'Creacion y gestion microempresas', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'Competencia matematica de nivel 2', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- 
-- Dumping data for table matriculas
--
INSERT INTO matriculas VALUES
(1, 1, 1, '2018-02-02', NULL),
(2, 1, 2, '2018-02-02', NULL),
(3, 2, 1, '2018-02-02', NULL),
(4, 2, 3, '2018-01-31', NULL);

