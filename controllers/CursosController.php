<?php

namespace app\controllers;

use Yii;
use app\models\Cursos;
use app\models\CursosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\web\UploadedFile;

/**
 * CursosController implements the CRUD actions for Cursos model.
 */
class CursosController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Cursos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CursosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionListar()
    {
        
        $dataProvider = new ActiveDataProvider([
            'query' => Cursos::find(),
        ]);
       
        return $this->render('listar',[
            'dataProvider'=>$dataProvider,
        ]);
    }
    
    /**
     * Displays a single Cursos model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Cursos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

     public function actionCreate() {
        $model = new Cursos();

        if ($model->load(Yii::$app->request->post())) {
            $model->cartel = UploadedFile::getInstance($model, 'cartel');
            $model->informacion = UploadedFile::getInstance($model, 'informacion');
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }


    public function actionEliminar($id,$tipo){
        $model = $this->findModel($id);
        $model->$tipo="";
        $model->save();
        return $this->redirect(['view', 'id' => $model->id]);
    }
    
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        if ($model->load(Yii::$app->request->post())) {
            // intentamos recuperar el fichero subido (actualizado)
            // en caso de que no seleccione ninguno el metodo estatico
            // devuelve null
            $model->cartel = UploadedFile::getInstance($model, 'cartel');
            $model->informacion = UploadedFile::getInstance($model, 'informacion');

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }


    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Cursos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Cursos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Cursos::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    /**
     * Nos permite realizar la subida de archivos
     * @return boolean
     */
    
    public function upload()
    {
        if ($this->validate()) {
            $this->foto->name=$this->nombre . $this->foto->name;
            $this->foto->saveAs('imgs/' . $this->foto->name,false);
            return true;
        } else {
            return false;
        }
    }
}
