<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Alert;

/* @var $this yii\web\View */
/* @var $model app\models\Cursos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cursos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'especialidad')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nivel')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dirigido')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'modalidad')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'turno')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cartel')->fileInput() ?>

    <?php
    if (!empty($model->cartel)) {
        echo Alert::widget([
            'body' => Html::img($model->getCartel(), [
                'class' => 'img-responsive img-thumbnail',
            ]),
            'closeButton' => [
                'tag' => 'a',
                'href' => \yii\helpers\Url::to(['cursos/eliminar', 'id' => $model->id, 'tipo' => 'cartel']),
            ],
        ]);
    }
    ?>

    <?= $form->field($model, 'informacion')->fileInput() ?>

    <?php
    if (!empty($model->informacion)) {
        echo Alert::widget([
            'body' => '<embed src="' . $model->getInformacion() . '" width="500" height="200">',
            'closeButton' => [
                'tag' => 'a',
                'href' => \yii\helpers\Url::to(['cursos/eliminar', 'id' => $model->id, 'tipo' => 'informacion']),
            ],
        ]);
    }
    ?>



    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
